package ru.nlmk.study.jse51.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.nlmk.study.jse51.mvc.service.MobileService;

@Controller
public class MobileController {
    MobileService mobileService;

    @Autowired
    public void setMobileService(MobileService mobileService) {
        this.mobileService = mobileService;
    }

    @RequestMapping("/mobile/hello")
    public String hello(
            @RequestParam(defaultValue="", required=false) String mobileModel,
            @RequestParam(defaultValue="0", required=false) Integer mobilePrice,
            Model model){
        model.addAttribute("name", mobileService.createMobile(mobileModel));
        model.addAttribute("price", mobilePrice);
        if (mobileService != null) {
            model.addAttribute("check", "OK");
        } else {
            model.addAttribute("check", "no service");
        }
        return "hello";
    }

    @RequestMapping("/mobile/mobileAdd")
    public String mobileAdd(Model model){
        return "mobileAdd";
    }
}

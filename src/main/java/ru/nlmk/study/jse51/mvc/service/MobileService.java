package ru.nlmk.study.jse51.mvc.service;

import org.springframework.stereotype.Service;

@Service
public class MobileService {
    public String createMobile(String mobileName){
        return mobileName.toUpperCase();
    }
}

package ru.nlmk.study.jse51.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ShopController {
    @RequestMapping("/shop/hello")
    public String hello(
            @RequestParam(defaultValue="", required=false) String mobileModel,
            Model model){
        model.addAttribute("name", mobileModel);
        model.addAttribute("check", "OK");
        return "hello";
    }
}
